import java.util.Random;

public class RouletteWheel {
    private Random rng;
    private int number;

    public RouletteWheel(){
        rng = new Random();
        number = 0;
    }

    public void spin(){
        number = rng.nextInt(37);
    }

    public int getValue(){
        return number;
    }

    public boolean isLow(){
        return (number >= 1 && number <= 19);
    }

    public boolean isHigh(){
        return (number >= 19 && number <= 36);
    }

    public boolean isEven(){
        return (number % 2 == 0) && number != 0;
    }

    public boolean isOdd(){
        return !isEven();
    }

    public boolean isRed(){
        if((number >= 1 && number <= 10) || (number >= 19 && number <= 28))
            return isOdd();
        else if(number != 0)
            return isEven();
        return false;
    }

    public boolean isBlack(){
        return !isRed() && number != 0;
    }
}
