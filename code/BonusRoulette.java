import java.util.Scanner;

//Bonus roulette
public class BonusRoulette {
    public static void main(String[] args){
        RouletteWheel roulette = new RouletteWheel();
        Scanner kb = new Scanner(System.in);

        int balance = 1000;
        int bet = 0;
        int number = 0;
        boolean gameOver = false;
        String replayAnswer = "";
        char kindOfBet = '-';

        while(!gameOver && balance > 0){
            bet = 0;
            replayAnswer = "";

            do{
                System.out.println("Current balance : " + balance);
                System.out.println("How much would you like to bet? (bet must be < " + balance + " and > 0)");
                bet = kb.nextInt();
                balance -= bet;
            }while(bet > balance || bet < 0);
            if(bet > 0){
                System.out.println("What kind of bet would you like to make (number, low, high, even, odd, red, black)?");
                kindOfBet = kb.nextLine().toLowerCase().charAt(0);

                if(kindOfBet == 'n'){
                    do{
                        System.out.println("What number would you like to bet on? (0-36)");
                        number = kb.nextInt();
                    }while(number < 0 && number > 36);
                }

                roulette.spin();

                if(kindOfBet == 'n' && roulette.getValue() == number){
                    System.out.println("You won " + bet * 35 + "!");
                    balance += bet * 35;
                }else if(kindOfBet == 'l' && roulette.isLow()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else if(kindOfBet == 'h' && roulette.isHigh()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else if(kindOfBet == 'r' && roulette.isRed()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else if(kindOfBet == 'b' && roulette.isBlack()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else if(kindOfBet == 'e' && roulette.isEven()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else if(kindOfBet == 'o' && roulette.isOdd()){
                    System.out.println("You won " + bet * 2 + "!");
                    balance += bet * 2;
                }else {
                    System.out.println("You lost " + bet);
                }
            }
            System.out.println("Would you like to continue playing? (y/n)");
            replayAnswer = kb.next();
            if(!replayAnswer.equalsIgnoreCase("y"))
                gameOver = true;
        }

        System.out.println("You " + ((balance > 1000) ? "won ":"lost ") + Math.abs(balance - 1000));
    }
}