import java.util.Scanner;

//Basic roulette
public class Roulette {
    public static void main(String[] args){
        RouletteWheel roulette = new RouletteWheel();
        Scanner kb = new Scanner(System.in);

        int balance = 1000;
        int bet = 0;
        int number = 0;
        boolean gameOver = false;
        String replayAnswer = "";

        while(!gameOver && balance > 0){
            bet = 0;
            replayAnswer = "";
            do{
                System.out.println("Current balance : " + balance);
                System.out.println("How much would you like to bet? (bet must be < " + balance + " and > 0)");
                bet = kb.nextInt();
                balance -= bet;
            }while(bet > balance || bet < 0);
            if(bet > 0){
                do{
                    System.out.println("What number would you like to bet on? (0-36)");
                    number = kb.nextInt();
                }while(number < 0 && number > 36);

                roulette.spin();

                if(roulette.getValue() == number){
                    System.out.println("You won " + bet * 35 + "!");
                    balance += bet * 35;
                }else{
                    System.out.println("You lost " + bet + "!");
                }
            }
            System.out.println("Would you like to continue playing? (y/n)");
            replayAnswer = kb.next();
            if(!replayAnswer.equalsIgnoreCase("y"))
                gameOver = true;
        }

        System.out.println("You " + ((balance > 1000) ? "won ":"lost ") + Math.abs(balance - 1000));
    }
}